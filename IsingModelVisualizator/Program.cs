﻿using System;
using System.Drawing;
using Excel = Microsoft.Office.Interop.Excel;

/*
 * This project needs nuget package: Microsoft.Office.Interop.Excel
 * You also need to add reference to System.Drawing
 *  
 *  protip: after generating xlsx file resize all rows and columns of worksheet to 1 px for better performance and visual sensations :)
 */

namespace IsingModelVisualizator
{
    public class Program
    {
        private static Excel.Application _Excel;
        private static Excel.Workbook _WorkBook;
        private static Excel.Worksheet _WorkSheet;

        private static void ExcelConfiguration()
        {
            _Excel = new Excel.Application();
            _WorkBook = _Excel.Workbooks.Add();
            _WorkSheet = (Excel.Worksheet)_WorkBook.Sheets[1];
        }

        private static void ExcelSaveResults(string path)
        {
            _WorkBook.SaveAs(path);
            _WorkBook.Close();
        }

        private static void ExcelCellColorize(Excel.Worksheet worksheet, int x, int y, Color color)
        {
            //good to know, that excel starts cells numeration from 1 not zero
            worksheet.Cells[x, y].Interior.Color = color;
        }

        private static  string[] GetDataFromFile(string path)
        {
            return System.IO.File.ReadAllLines(path);
        }

        private static void DoTheMagic(string path)
        {
            var file = GetDataFromFile(path);

            Console.WriteLine("----Data processing is starting\n");

            foreach (var line in file)
            {
                string[] values = line.Split(',');
                int x = Int32.Parse(values[0]);
                int y = Int32.Parse(values[1]);

                //Wheat color is better than white to see where our model ends in spreadsheet
                ExcelCellColorize(_WorkSheet, x+1, y+1, values[2] == "black" ? Color.Black : Color.Wheat);
                Console.Write(".");
            }

            Console.WriteLine("\n\n----Succesfully processed: " +file.Length+ " pixels");
        }

        public static void Main(string[] args)
        {
            try
            {
                ExcelConfiguration();

                //Data should be in this strict format <x,y,colorName>, fill with your full path to file
                string dataPath = @"C:\Users\...\Desktop\T2-400.txt";
                DoTheMagic(dataPath);

                //out path, place where you want to see spreadsheet
                ExcelSaveResults(@"C:\Users\...\Desktop\isingModel.xlsx");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
            }
        }
    }
}
