# Ising Model

>
"The Ising model [..], named after the physicist Ernst Ising, is a mathematical model of ferromagnetism in statistical mechanics. The model consists of discrete variables that represent magnetic dipole moments of atomic spins that can be in one of two states (+1 or −1). The spins are arranged in a graph, usually a lattice, allowing each spin to interact with its neighbors. The model allows the identification of phase transitions, as a simplified model of reality. The two-dimensional square-lattice Ising model is one of the simplest statistical models to show a phase transition" 
by wikipedia [https://en.wikipedia.org/wiki/Ising_model]
>

## Check also another branches to see more: [evaluation](https://gitlab.com/winmaster_code/IsingModel/tree/evaluation) and [GUI](https://gitlab.com/winmaster_code/IsingModel/tree/GUI) where you can find generating into .jpg file and Winforms GUI

### The case was to visualize (for ex. in chart), the output of series of physics calculation based on temperature of system and another physicis stuff that I don't even understand :monkey_face:

The output dataset should fill the pattern: 
x, y - coordinates, and color -  representation of spin value (+1 or -1) expressed for example by 'Color'. 
In my representation +1 is black `#000000`, and -1 is white `#FFFFFF`

Here is a sample dataset with Temperature (T) = 2 and Matrix size (N) = 400

## T_2_400 dataset:

```
0,0,black
0,1,black
0,2,black
0,3,black
0,4,black
0,5,black
0,6,black
0,7,black
0,8,black
0,9,black
0,10,black
0,11,black
0,12,black
0,13,black
0,14,black
0,15,white
0,16,black
0,17,black
0,18,black

.....

399,387,white
399,388,white
399,389,black
399,390,black
399,391,white
399,392,black
399,393,black
399,394,black
399,395,black
399,396,black
399,397,black
399,398,white
399,399,black

```

it is available in full version here [file](/data/T2-400.txt)


### The real problem was, that natural tool like excel to make plot of this data  was insufficient due to its amount of records constraints.  As it it known from literature, the Ising's model output looks more like a two-colored texture not like a plot, so I came up with idea to use Excel as a tool to "draw" a painting of this model :)

### protip: after generating xlsx file resize all rows and columns of worksheet to 1 px for better performance and visual sensations :) 
Yes.. It can be done automatically via code, but I was lazy :snail: 
Generating spreadsheet takes for ex for N = 400 ca. 8-9 minutes.  It grows when the Size of Matrix is bigger. 


### Results are pretty cool:


## Screens

| Temp 1 N 400 | Temp 2 N 400 |
| ------------ | ------------ |
|![](screens/T_1_400.png)|![](screens/T_2_400.png)|

| Temp 3 N 400 | Temp 4 N 400 | Temp 5 N 400 |
| ------------ | ------------ | ------------ |
|![](screens/T_3_400.png)|![](screens/T_4_400.png)|![](screens/T_5_400.png)|


| Temp critical N 600 |
| ------------------- |
|![](screens/T_kryt_600.png) |

I see :hushed: in this... :rainbow:


### Pure effect of view is made by exporting xlsx file to pdf

Natural it looks like this:

![Excel View](screens/raw.png)



You don't have to be a physics hero, to be a winmaster :star2:

Enjoy this solution in your academic projects :)